#  deck of cards

deck = {}

def add_card(key, name, value, color, type):

    list = []
    if (key in deck):
        print "Key not unique !"
        return
    else:
        if (name != None and value != None and color != None and type != None):
            list.append(name)
            list.append(value)
            list.append(color)
            list.append(type)

            deck[key] = list
        else:
            print "Invalid value parameters!"

def check_cards(key1, key2):
    if key1 in deck and key2 in deck:
        if deck[key1][1] > deck[key2][1]:
            print "The highest card is :", deck[key1]
        elif deck[key1][1] < deck[key2][1]:
            print "The highest card is :", deck[key2]
        else:
            print "Equal cards !"
    else:
        print "Invalid parameters!"
        return

def print_card(key, name, value, color, type):
    if deck[key] != None :
        print "Name: ",deck[key][0], "; value : ", deck[key][1], "; color : ", deck[key][2], "; type :", deck[key][3]
    else:
        print "Invalid parameters !"

def main():
    add_card(1, "ace", 1, "red", "hearts")
    add_card(2, "king", 13, "red", "hearts")
    check_cards(1, 2)

    print deck

main()
