def first(mylist):
    "This changes a passed list into this function"
    mylist.append([1, 2, 3, 4])
    print "Values inside the function: ", mylist
    return


def second(mylist):
    "This changes a passed list into this function"
    mylist = [1, 2, 3, 4]  # This would assig new reference in mylist
    print "Values inside the function: ", mylist
    return

# mylist = [10, 20, 30]
# first(mylist)
# print "Values outside the function: ", mylist
mylist = [10, 20, 30]
second(mylist)
print "Values outside the function: ", mylist