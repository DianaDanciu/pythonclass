__author__ = 'foleac'

import logging

class Logger(object):

    def __init__(self, logFile):
        logging.basicConfig(filename=logFile, level=logging.ERROR, format='%(asctime)s %(message)s')

    def debug(self, message):
        logging.debug(message)

    def critical(self, message):
        logging.critical(message)

    def info(self, message):
        logging.info(message)

    def error(self, message):
        logging.error(message)

    def warning(self, message):
        logging.warning(message)

